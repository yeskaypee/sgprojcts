<!DOCTYPE html>
<html lang="en">
<head>
  <title>BINS DEMO</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
<br>
  <div class="row">
    <div class="col-sm-12">
      <div class="col-sm-1"></div>
      <div class="col-sm-10">
  <form class="form-horizontal bg-danger" method="POST">
    <div class="row">
     

     <div class="col-sm-1"></div>
     <div class="col-sm-2">
      <h4 class="text-primary">BINS</h4>
       <div class=" row">
      <div class=" col-sm-10">
         <textarea class="form-control" rows="3" name="bins" id="bins"><?php if(!empty($_POST['bins'])) echo $_POST['bins']; ?></textarea>
      </div>
    </div>
    </div>

    <div class="col-sm-2">
    <div class="form-group">
      <h4 class="text-primary">VENDOR</h4>
      
      <div class="col-sm-12">
        <div class="row">
          <label class="checkbox-inline"><input type="checkbox" name="vendor[]" value="visa" <?php //if(!empty($_POST['vendor']) && $_POST['vendor']=="visa") echo "checked"; ?>> Visa</label>
       
        
          <label class="checkbox-inline"><input type="checkbox" name="vendor[]" value="mc"> MC</label>
        </div>
       
         <div class="row">
          <label class="checkbox-inline"><input type="checkbox" name="vendor[]" value="amex"> Amex</label>
       
         
          <label class="checkbox-inline"><input type="checkbox" name="vendor[]" value="maestro"> Maestro</label>
        </div>
       
         <div class="row">
          <label class="checkbox-inline"><input type="checkbox" name="vendor[]" value="dicover"> Discover</label>
        
       
          <label class="checkbox-inline"><input type="checkbox" name="vendor[]" value="dci"> DCI</label>
        </div>
        <div class="row">
          <label class="checkbox-inline"><input type="checkbox" name="vendor[]" value="jcb"> JCB</label>
        
          <label class="checkbox-inline"><input type="checkbox" name="vendor[]" value="unionpay"> UNIONPAY</label>
        </div>
      </div>
      </div>
      </div>
    
    <div class="col-sm-3">
    <h4 class="text-primary">LEVEL</h4>
      
      <div class="col-sm-12">
        <div class="row">
          <label class="checkbox-inline"><input type="checkbox" name="level[]" value="classic" <?php //if($_POST['level']=="classic") echo "checked"; ?>> Classic/Std.</label>
       
        
          <label class="checkbox-inline"><input type="checkbox" name="level[]" value="business"> Business</label>
        </div>
       
         <div class="row">
          <label class="checkbox-inline"><input type="checkbox" name="level[]" value="gold"> Gold/Prem</label>
       
         
          <label class="checkbox-inline"><input type="checkbox" name="level[]" value="corporate"> Corporate</label>
        </div>
       
         <div class="row">
          <label class="checkbox-inline"><input type="checkbox" name="level[]" value="platinum"> Platinum</label>
        
       
          <label class="checkbox-inline"><input type="checkbox" name="level[]" value="infinite"> Infinite</label>
        </div>
        <div class="row">
          <label class="checkbox-inline"><input type="checkbox" name="level[]" value="signature"> Signature</label>
        
          <label class="checkbox-inline"><input type="checkbox" name="level[]" value="cash"> Cash</label>
        </div>
        <div class="row">
          <label class="checkbox-inline"><input type="checkbox" name="level[]" value="electron"> Electron</label>
        
       
          <label class="checkbox-inline"><input type="checkbox" name="level[]" value="purchasing"> Purchasing</label>
        </div>
        <div class="row">
          <label class="checkbox-inline"><input type="checkbox" name="level[]" value="prepaid"> Prepaid</label>
        
          <label class="checkbox-inline"><input type="checkbox" name="level[]" value="virtual"> Virtual</label>
        </div>
        
      </div>
      </div>
    
    <div class="col-sm-3">
      <h4 class="text-primary">REGION</h4>
       <div class=" row">
      <label class="control-label col-sm-4" for="bank">BankName:</label>
      <div class=" col-sm-8">
        <input type="text" class="form-control" name="bank" value="<?php if(!empty($_POST['bank'])) echo $_POST['bank']; ?>">
      </div>
    </div>
    
   <div class=" row">
      <label class="control-label col-sm-4" for="country">Country:</label>
      <div class=" col-sm-8">
        <input type="text" class="form-control" name="country" value="<?php if(!empty($_POST['country'])) echo $_POST['country']; ?>">
      </div>
    </div>
    
    <h4 class="text-primary">TYPE</h4>
      
      <div class="col-sm-10">
        <div class="row">
          <label class="checkbox-inline"><input type="checkbox" name="type[]" value="CREDIT"> Credit</label>
       
        
          <label class="checkbox-inline"><input type="checkbox" name="type[]" value="debit"> Debit</label>
        </div>
       
         <div class="row">
          <label class="checkbox-inline"><input type="checkbox" name="type[]" value="charge"> Charge</label>
       
         
          <label class="checkbox-inline"><input type="checkbox" name="type[]" value="others"> Others</label>
        </div>
        </div>
        </div>
        </div>


    <div class="form-group" style="padding-bottom: 5px; padding-top: 10px;"> 
    <div class="col-sm-5"></div>       
        <input type="reset" class="btn btn-default btn-lg" value="Reset" name="reset" id="reset">
     
        <input type="submit" class="btn btn-primary btn-lg" value="Submit" name="submit">
    </div>
   
  </form>
</div>
</div>
</div>
</div>

<br>
<div class="container">


      <?php

if(isset($_POST['submit'])){

  ?>
<table class="table">
    <thead>
      <tr class="bg-primary">
        <th>Bins</th>
        <th>Brand</th>
        <th>Sub-brand</th>
        <th>Type</th>
        <th>Subtype</th>
        <th>Bank</th>
        <th>Country</th>
        <th>Country Code</th>
        <th>Latitude</th>
        <th>Longitude</th>
      </tr>
    </thead>
    <tbody>
  <?php
  $searchkey = [] ;
  
//   $data='111,222';
// $splittedstring=explode(" ",$data);
// foreach ($splittedstring as $key => $value) {
// echo "splittedstring[".$key."] = ".$value."<br>";
// }

    if(!empty($_POST['bins'])){
    // if (is_array($_POST['bins']))
    // {
    $bins = $_POST['bins'];
    //echo $bins;
    $binsvalue = explode("\r\n", $_POST['bins']);;
    //var_dump($binsvalue);
    //echo $binvalue;
    foreach($binsvalue as $b => $b1) {
    $searchkey['bins'][] = $b1;
    
}
    
  }
//  }

  

  if(!empty($_POST['vendor'])){
    $vendor = array($_POST['vendor']);
    foreach($_POST['vendor'] as $vendor_list => $v){
    $vendor = $v;
    $searchkey['vendor'][] = $vendor ;
    //array_push($searchkey, $vendor);
 
}
}


if(!empty($_POST['level'])){
  $level = array($_POST['level']);
foreach($_POST['level'] as $level_list => $l){
  $level = $l;
  $searchkey['level'][]= $level;
 
}
}

if(!empty($_POST['bank'])){
    $bank = $_POST['bank'];
    $searchkey['region']['bank'][] = $bank;
    //array_push($searchkey, $bank);
  }
  
   if(!empty($_POST['country'])){
    $country = $_POST['country'];
   // array_push($searchkey, $country);
    $searchkey['region']['country'][] = $country;
  }

  if(!empty($_POST['type'])){
  $type = array($_POST['type']);
foreach($_POST['type'] as $type_list => $t){
    $type = $t;
    $searchkey['type'][]= $type;
}
       // echo "<pre>";
       // print_r($searchkey);
       // echo "</pre>";
}

      $searchkey1 = [];
function recursive_loop($array)
    {
        foreach($array as $key => $value)
        {
            if(is_array($value))
            {
                recursive_loop($value);
            }
            else
            {
                echo $value . " ";// PHP_EOL;
            }
        }
        //$searchkey1 =  recursive_loop($searchkey);
    }
   $searchkey1 =  recursive_loop($searchkey);

     echo "<pre>";
       print_r($searchkey1);
       echo "</pre>";

if (($handle = fopen("test.csv", "r")) !== FALSE) {
   
    $row = 0;
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
    if (in_array($searchkey1, $data)) {
      $row++;
    }
    }
      echo "<h4>Total found bins : ". "<b>".$row."</b><h4>";
      fclose($handle);
    }
     


    if (($handle1 = fopen("test.csv", "r")) !== FALSE) {
    while (($data1 = fgetcsv($handle1, 1000, ",")) !== FALSE) {      
    if (in_array($searchkey1, $data1)) {
            echo "<tr>";
           foreach ($data1 as $cell) {
                echo "<td>" . htmlspecialchars($cell) . "</td>";
        }
            
            echo "</tr>";
            }
    }
    }
    fclose($handle1);
    
 }

?>
    </tbody>
  </table>
</div>
</body>
</html>
